import React from 'react'
import {
    Marker,
    Popup
} from 'react-leaflet'


// контенер маркера для карты
export default class Markermap extends React.Component{
    render(){
        let conf = this.props.conf
        let x, y
        try{
            x = parseFloat(conf.latitude)
            y = parseFloat(conf.longitude)
        } catch(err) {
            console.log('Wrong coordinates: ' + err)
            x = false
            y = false
        }
        return(
            <React.Fragment>
                {
                    x && y ? (
                        <Marker
                            position={[x, y]}
                            onClick={() => this.props.edit()}
                        />
                    ) : false
                }
            </React.Fragment>
        )
    }
}

