import 'core-js'
import React from "react"
import ReactDOM from "react-dom"
import './../styles/main.scss'
import Index from "./../components/Index"


// точка входа в реакт
ReactDOM.render(
    <Index />, 
    document.getElementById("root")
)