// any app functions

// save to localstorage
export function savestorage(points){
    let str = JSON.stringify(points)
    localStorage.setItem('mappoints', str)
}

// get points from localstorage
export function getstorage(){
    let points 
    let str = localStorage.getItem('mappoints')
    if(str){
        points = JSON.parse(str)
    }
    return points
}