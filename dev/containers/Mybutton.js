import React from 'react'

// контейнер кнопоньки
export default class Mybutton extends React.Component{
    render(){
        return(
            <button
                className='btn-add'
                onClick={() => this.props.onclick()}
            >
                {this.props.text}
            </button>
        )
    }
}