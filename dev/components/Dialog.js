import React from 'react'
import moment from 'moment'
import Mybutton from '../containers/Mybutton'


// диалог добавления/редактирования точек карты
export default class Dialog extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            name: '',
            date: '',
            latitude: '',
            longitude: ''
        }
    }

    UNSAFE_componentWillReceiveProps(np){
        if(np.open && this.props.open != np.open){
            if(np.editdata){
                this.setState(np.editdata)
            } else {
                this.setState({ date: moment().valueOf() })
            }
        } else {
            this.setState({
                name: '',
                date: '',
                latitude: '',
                longitude: ''
            })    
        }
    }

    // сохранить точку диалога
    savePoint(){
        let point = {
            name: this.state.name,
            date: this.state.date,
            latitude: this.state.latitude,
            longitude: this.state.longitude
        }
        if(point.name == ''){
            console.log('The name not inserted')
        } else {
            if(this.props.editdata){
                this.props.editpoint(
                    Object.assign({}, point, {id: this.props.editdata.id})
                )
            } else {
                this.props.addpoint(point)
            }
        }
        
    }

    render(){
        return(
            <React.Fragment>
                {
                    this.props.open ? (
                        <div
                            className='dialog'
                        >
                            <div
                                style={{
                                    float: 'right'
                                }}
                            >
                                <p
                                    onClick={() => this.props.close()}
                                >
                                    X
                                </p>
                            </div>
                            <p>Создание/Редактирование точки</p>
                            <p>
                                <input 
                                    className='txt-input'
                                    type='text' 
                                    placeholder='название точки'
                                    value={this.state.name}
                                    onChange={(ev) => this.setState({ name: ev.target.value })}
                                />
                            </p>
                            <p>
                                <input 
                                    className='txt-input'
                                    type='text' 
                                    value={moment(this.state.date).format('DD-MM-YYYY HH:mm:ss')}
                                    placeholder='дата и время создания'
                                    readOnly
                                />
                            </p>
                            <p>
                                <input 
                                    className='txt-input'
                                    type='text' 
                                    placeholder='широта'
                                    value={this.state.latitude}
                                    onChange={(ev) => this.setState({ latitude: ev.target.value })}
                                />
                            </p>
                            <p>
                                <input 
                                    className='txt-input'
                                    type='text' 
                                    placeholder='долгота'
                                    value={this.state.longitude}
                                    onChange={(ev) => this.setState({ longitude: ev.target.value })}
                                />
                            </p>
                            <p>
                                <Mybutton 
                                    text='Сохранить'
                                    onclick={() => this.savePoint()}
                                />
                            </p>
                        </div>
                    ) : false
                }
            </React.Fragment>
        )
    }
}