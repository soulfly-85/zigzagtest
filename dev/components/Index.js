import React from 'react'
import { 
    Map, 
    TileLayer,
} from 'react-leaflet'

import Control from './Control'
import Dialog from './Dialog'
import Markermap from './../containers/Markermap'
import { savestorage, getstorage } from '../functions/funct'


// главный компонент
export default class Index extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            open: false,
            points: [],
            editdata: false,
            filtertxt: '',
            filterpoints: false
        }
    }

    componentDidMount(){
        this.getPoints()
    }

    // достать точки из localstorage
    getPoints(){
        let points = getstorage()
        points ? this.setState({ points: points }) : false
    }

    // сохранить новую точку в состояние
    savePoint(data){
        let points = this.state.points
        points.push(
            Object.assign({}, data, {id: points.length + 1 })
        )
        this.setState({ points: points, open: false, editdata: false }, () => savestorage(points))
    }

    // открыть диалог редактирования точки
    editData(id){
        let ep = this.state.points.find( x => x.id == id )
        this.setState({ editdata: ep, open: true })
    }

    // сохранить редактированну точку в сотояние
    editPoint(data){
        let points = this.state.points
        let curidx = points.findIndex( x => x.id == data.id)
        points[curidx] = data
        this.setState({ points: points, open: false, editdata: false }, () => savestorage(points))
    }

    // изменение фильтра
    changeFilter(txt){
        // просто содержание текста в имени точки (неважно в каком месте совпадение)
        let filterpoints = this.state.points.filter( x => x.name.includes(txt))
        this.setState({ filtertxt: txt, filterpoints })
    }   

    render(){
        // в зависимости от фильтра показываем точки
        let accesspoints = this.state.filtertxt == '' ? this.state.points : this.state.filterpoints
        return(
            <div
                className='fullblk rel-position'
            >
                <Map 
                    center={[56.007870, 92.853774]} 
                    zoom={12}
                    zoomControl={false}
                >
                    <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {
                        accesspoints.map((p, id) => {
                            return (
                                <Markermap 
                                    key={id}
                                    conf={p} 
                                    edit={() => !this.state.open ? this.editData(p.id) : false}
                                />
                            )
                        })
                    }
                </Map>
                <Control 
                    open={this.state.open}
                    points={accesspoints}
                    setopen={() => this.setState({ open: true })}
                    editdata={(id) => this.editData(id)}
                    filtertxt={this.state.filtertxt}
                    changefilter={(txt) => this.changeFilter(txt)}
                />
                <Dialog 
                    open={this.state.open}
                    close={() => this.setState({ editdata: false, open: false })}
                    addpoint={(data) => this.savePoint(data)}
                    editpoint={(data) => this.editPoint(data)}
                    editdata={this.state.editdata}
                />
            </div>
        )
    }
}

