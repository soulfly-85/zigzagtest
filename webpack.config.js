const path = require("path")

module.exports = {
    entry: {
        bundle: './dev/entry/index.js',
    },
    output: {
        path: path.join(__dirname, "/"),
        filename: "[name].js"
    },
    performance: {
		maxEntrypointSize: 6144000,
		maxAssetSize: 6144000
    },
    devServer: {
        hot: true
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader",
                options: {
                    presets: ["@babel/preset-env", "@babel/preset-react"]
                }
            }
        },{
            test: /\.(css|scss)$/,
            exclude: /node_modules/,
            use: [{
                    loader: 'style-loader'
                },{
                    loader: 'css-loader'
                },{
                    loader: 'sass-loader'
                }]
        },{
            test : /\.(jpg|png|svg)$/,
            exclude: /(node_modules)/,
            loader : 'url-loader'
        }]
    }
}