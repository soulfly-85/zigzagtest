import React from 'react'

import Mybutton from '../containers/Mybutton'


// блок управления точками
export default class Control extends React.Component{
    render(){
        return(
            <div
                className='control-div'
            >
                <div
                    className='btn-div'
                >
                    <Mybutton 
                        text='Добавить точку'
                        onclick={() => this.props.setopen()}
                    />
                </div>
                <div
                    className='point-list'
                >
                    <p>
                        <input 
                            type='text' 
                            placeholder='Фильтр по имени...'
                            className='txt-input'
                            value={this.props.filtertxt}
                            onChange={(ev) => this.props.changefilter(ev.target.value)}
                        />
                    </p>
                    <div
                        className='created-points'
                    >
                        {
                            this.props.points.map((p, idp) => {
                                return(
                                    <p 
                                        key={idp}
                                        onClick={() => !this.props.open ? this.props.editdata(p.id) : false}
                                    >
                                        {p.name}
                                    </p>
                                )
                            })
                        }
                    </div>
                </div>
                
            </div>
        )
    }
}